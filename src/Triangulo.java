
import java.awt.Color;
import java.awt.Point;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author macpro1
 */
public class Triangulo extends Figura {

    Point p1, p2, p3;

    public Triangulo(Point p1, Point p2, Point p3, Color c) {
        this.p1 = new Point(p1.x, p1.y);
        this.p2 = new Point(p2.x, p2.y);
        this.p3 = new Point(p3.x, p3.y);
        this.color = c;
    }

}
